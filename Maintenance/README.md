# Maintenance 🛠️

Maintenance service document.

## Monthly maintenance

**Monthly** the **RIG** need cleaning to remove all dust for guaranties the correct work. 

For this maintenance we need:

- Anti-static brush.
- Air compressor for PC's.
- Anti-static microfiber.
- Aprox. 1 hour maintenance window.

The procedure isn't a science, you need to clean like a computer, be careful with static, and don't move fans with the air. 

1. Shut down the system and unplug the power. 
2. Wait at least 20 minutes to cool down.
3. Use air to remove dust.
4. Use a brush and microfiber to remove fluff and extra dust.
5. Again use air and done.

For reference simply Google it, the most important thing is to do it regularly. [**Our reminder**](https://calendar.google.com/event?action=TEMPLATE&tmeid=NGFkNmV2aDZ0cjQwa2FlZ2xibTRrcGw3bTJfMjAyMjAxMDdUMjMwMDAwWiBnYWJyaWVsbEBkZXZvZGRzLmNvbQ&tmsrc=gabriell%40devodds.com&scp=ALL)
