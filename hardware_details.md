# Hardware details

Detailed information about hardware in the **RIG**

- Rack: `Frame for 6 GPU` [Amazon product](https://www.amazon.com.mx/gp/product/B095YLMHSG/ref=ppx_yo_dt_b_asin_image_o01_s00?ie=UTF8&th=1)

![](ethereum_rig.gif)

## Hardware label

The **RIG** has an identification label, that label was printed with **Zebra Printer** and added to the frame.

- [**Hardware Label image**](HardwareLabel.PNG).
- [**Source label file**](HardwareLabel.lbl) for Zebra Printer software.

## GPU-1

- Model: `ZOTAC GeForce RTX 3070 8GB TWIN EDGE`
- PN: `9288-5N617-310Z8`
- SKU: `ZT-A30700H-10P`
- SN: `N211900001692`

## GPU-2

- Model: `ZOTAC GeForce RTX 3070 8GB TWIN EDGE`
- PN: `9288-5N617-310Z8`
- SKU: `ZT-A30700H-10P`
- SN: `N212000014335`

## GPU-3

- Model: `ZOTAC GeForce RTX 3070 8GB TWIN EDGE`
- PN: `9288-2N617-310Z8`
- SKU: `ZT-A30700H-10P`
- SN: `N211200012189`

## HDMI Dummy

The motherboard has **HDMI Dummy** connected for a correct system boot, without this, the BIOS doesn't boot properly. 

## Funs

Instaled in the RIG frame has 3 fans for cooling, we use [**BALAM RUSH - 120 Mm EOLOX AIR 3 AFX30**](https://balamrush.com/componentes/49-eolox-air-3-afx30.html)
