# Rig RTX 3070 Ethereum

Our first **GPU RIG** for [**Ethereum**](https://ethereum.org/en/) with [**Minerstat**](https://minerstat.com/) ![](ethereum_icon.gif)

This project is to show and share our experience and work mining **Ethereum** with GPU's, please feel free to use all information for your own project without asking permission. 

We started this project with the idea to learn and play, we love technology and we were boring so here is.

## Hardware

Hardware list:

- Rack: `Frame for 6 GPU`
- Motherboard: `Motherboard GIGABYTE H410M H, DDR4, Intel, LGA 1200, Micro ATX`
- CPU: `Intel Celeron G5905, 3.50GHz`
- RAM: `RAM ADATA 8 GB, DDR4, 2400 MHz, 288-pin DIMM`
- SSD: `ADATA SSD 250 GB`
- Power supply: `Balam Rush GR850FM 80 PLUS Gold 850 W`
- GPU1: `ZOTAC GeForce RTX 3070 8GB` with raiser PCI-e
- GPU2: `ZOTAC GeForce RTX 3070 8GB` with raiser PCI-e
- GPU3: `ZOTAC GeForce RTX 3070 8GB` with raiser PCI-e

> See hardware details in: [Hardware details file](hardware_details.md)

> [Google Photos Album](https://photos.app.goo.gl/5G962pFYzbvCb84a8)

## Monitoring

### Mining monitoring

We use [**Minerstat**](https://minerstat.com/) software for mining and [**Ethermine**](https://ethermine.org/) pool. In **Ethermine** we use our **Bitso Wallet** for **ETH** `0x1f5782017e6eb5b9876d8bed0da7c374987625fb`.

### Infraestructure monitoring

The complete infraestructure in the **RIG** is monitored with [**Prometheus**](https://prometheus.io/) and [**Grafana**](https://grafana.com/) using **Grafana Cloud Service**

- Node monitoring: [**default dashboard**](https://devodds.grafana.net/d/bGMM6Gk7k/nodes?orgId=1&from=1626800478859&to=1626802278859) for CPU resources.

### Alertmanager

We receive some alerts from **Prometheus alert manager** to create a new issue in this project. These alerts can review in [**devodds-alertmanager**](https://devodds.grafana.net/a/grafana-alerting-ui-app/?tab=rules&rulessource=grafanacloud-devodds-prom&alertmanager=grafanacloud-devodds-alertmanager)

### Grafana alerts

Also we use **Grafana alerts** integrated with internal **Google Chat** in order to stay in alert inmediatle about any incident. These alerts can review in [**alerting**](https://devodds.grafana.net/alerting/list)

## Maintenance

Periodically the **RIG** needs a maintenance service to guaranties the correct work. 

See [**maintenance document**](/Maintenance/README.md) for complete reference.
